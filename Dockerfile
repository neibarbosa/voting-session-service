FROM openjdk:8-jdk-alpine

VOLUME /tmp

ENV SPRING_DATASOURCE_URL jdbc:postgresql://localhost:5432/voting
ENV SPRING_DATASOURCE_USERNAME voting
ENV SPRING_DATASOURCE_PASSWORD voting

COPY target/voting-session-service*.jar /voting-session-service.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/voting-session-service.jar"]
