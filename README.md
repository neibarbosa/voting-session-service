# Voting Session Service

Microsserviço que implementa funções REST simples de um sistema de votação.

As funções da API podem ser verificadas pelo Swagger http://localhost:8080/swagger-ui.html

## Premissas

Assumiu-se que a entidade central é a **sessão de votação**. Ela é que possui uma pauta que será discutida, que será iniciada ou encerrada, e é ela que tem os resultados da apuração.

Apesar de esta documentação estar em português, o código-fonte segue a língua inglesa sempre que se tratar de conceitos universais como "voto", "sessão" ou "associado". Havendo conceitos tipicamente brasileiros como CPF, CNPJ, impostos e boletos (muito comuns em ambientes bancários), a preferência se dará pela nomenclatura brasileira consagrada.

## Funções do Microsserviço

### Cadastrar uma pauta para uma sessão de votação

Assumiu-se que a única informação necessária para cadastrar uma sessão de votação era o nome da pauta (referenciada no código como **agenda**). Pode-se pensar como uma melhoria que as *opções de voto* seriam uma informação extra a se considerar no cadastro.

Após o cadastro bem sucedido o usuário recebe um identificador da sessão, que deverá ser informado para as demais operações.

### Abrir uma sessão de votação em uma pauta

Antes que uma sessão receba votos é preciso que ela tenha sido aberta (ou iniciada). E uma vez aberta quaisquer tentativas de iniciá-la novamente resultarão em erro 400.

Caso não exista sessão com o identificador informado, a API retorna 404.

Para abrir a sessão, é preciso informar seu tempo de duração da sessão, ou seja, por quantos minutos ela estará aberta para receber votos após iniciada. Se a duração não for informada, o valor padrão de 1 minuto será utilizado.

A API também valida durações inválidas como não-numéricas, e números não-positivos. Um ponto de melhoria é poder definir uma duração textual que pudesse indicar *quantidade* + *unidade de tempo* seguindo um padrão parecido com o [ISO](http://support.sas.com/documentation/cdl/en/lrdict/64316/HTML/default/viewer.htm#a003169814.htm) (ou até mesmo o próprio). Exemplo: "3d" para três dias, "2sem" para duas semanas e por aí vai.

### Receber votos dos associados em pautas

Os modelos envolvidos no processo de votação são bem simples: para votar, são necessárias três informações: o identificador da **sessão**, o identificador do **associado** e a **opção** de voto escolhida. Neste projeto as únicas opções válidas são "yes" and "no" mas elas são validadas somente na camada web. A camada de serviço está preparada para lidar com várias opções de escolha.

Para efeitos práticos, o identificador do associado é seu CPF, apesar de esta nomenclatura propositalmente não estar explícita na API. A ideia é que o identificador possa ser qualquer outro documento, ou uma chave em uma base de associados.

A API valida se um associado já votou naquela sessão de votação, retornando erro 403 (Forbidden) caso tente votar mais de uma vez.

Outra validação feita no associado é a consulta de um serviço externo que determina se ele está apto a votar ou não. O voto só será permitido se o serviço externo retornar código 200. Qualquer outro resultado, 4xx, 5xx ou erro ao consultar impedirá o registro do voto.

Da perspectiva de quem consome o serviço de sessão de votação, não importa se consultamos serviços externos ou não. De modo que, se o serviço externo nos retornar 5xx ou se o serviço falhar em realizar a consulta, o retorno dado para o usuário será 5xx também.

O sigilo de votação é importante, mas para efeitos de simplificação neste projeto, a tabela de votos agrega as informações de sessão, associado e opção de voto. Uma sugestão de melhoria seria manter separados os votos de uma votação e os associados que participaram da votação. Cuidados extras para manter integridade entre quantidade de votantes e de votos seriam necessários.

Outra simplificação assumida neste projeto foi não criar uma base de associados. As informações do associado (ou, "a" informação visto que só temos seu identificador) estão na tabela de votos.

### Contabilizar os votos e dar o resultado da votação da pauta

Os votos de uma sessão podem ser requisitados a qualquer momento, não importando se a sessão já foi iniciada ou se já foi encerrada. O payload de retorno desta função GET contém uma representação da sessão de votação, com identificador, pauta, hora de abertura e encerramento, e também um mapa em que a chave é a opção de voto e o valor é a quantidade de votos recebidos.

Um ponto de melhoria nesta função seria criar uma estrutura própria para os "itens de resultado" ao invés de usar um mapa padrão.

Os resultados (parciais ou finais) de uma sessão podem ser consultados a qualquer momento por meio da API. Além disso, também existe uma task assíncrona que a cada 30 segundos (intervalo configurável), levanta todas as votações encerradas e publica os resultados em um broker de mensageria, o qual neste projeto foi o Kafka. Uma vez que os resultados de uma votação foram publicados, a votação é marcada como publicada e não será selecionada na próxima execução.

O projeto inclui um consumidor muito simples do tópico de resultados, apenas para evidenciar que tanto a publicação quanto o consumo acontecem.

## Stack

As seguintes tecnologias foram utilizadas no desenvolvimento deste projeto:

* Java 8
* Maven
* Spring Boot
* JPA
* PostgreSQL
* Liquibase
* Hibernate
* Kafka
* Docker
* Swagger
* Sleuth
* Wiremock
* AssertJ
* Mockito

## Arquitetura

Neste desenvolvimento a possibilidade de testar as diferentes camadas de maneira independente teve grande destaque. A maioria das classes de teste dispensam o contexto do Spring, garantindo uma execução muito rápida. As exceções à regra são os testes de controller, de repositório de dados e o serviço de consulta de CPF. E mesmo nesses casos, houve o cuidado de carregar o mínimo contexto necessário.

### Camada web

No controller, os parâmetros dos métodos, bem como path e query params, e os payloads são todos validados com anotações. Eventuais violações de regras são tratadas no [ExceptionHandler](https://bitbucket.org/neibarbosa/voting-session-service/src/master/src/main/java/sicredi/exercicio/nbc/controller/handler/VotingSessionExceptionHandler.java).

#### Desacoplamento

Uma preocupação foi enxergar a camada web como um "detalhe", um mero invólucro ao redor do "core" do serviço, onde estarão as regras de negócio. Não havendo regras de negócio na camada web o serviço estaria livre para obter requisições consumindo de um broker de mensageria ao invés de ser chamado via HTTP.

Essa preocupação com desacoplamento se materializa na forma de pares de classe de DTO Request/Response. Esses pares são convertidos para correspondentes de "negócio", os quais são totalmente alheios a tecnologia. Essa é uma abordagem que gera mais classes, mas que se paga no médio prazo. Além de tornar os testes unitários mais simples.

### Camada de dados

O modelo de persistência de dados é considerado parte do serviço e, portanto, está no mesmo repositório. O framework Liquibase foi escolhido por já tê-lo utilizado em outras ocasiões.

O banco de dados de produção é um PostgreSQL 11, mas nos testes unitários usou-se o H2. Cheguei a pesquisar a utilização de um PostgreSQL "embedded" mas dada a simplicidade do projeto julguei que neste caso não faria mal testar o código com um banco diferente do de produção.

Nos recursos de testes há dois arquivos SQL que inserem votos e sessões de votação e é em cima desse setup que os testes dos repositórios JPA rodam.

### Serviço externo de Consulta de CPF

O objetivo não é testar exatamente o próprio serviço de consulta, mas sim a maneira como o serviço de votação reage a ele. Para viabilizar esse teste, utilizei o Wiremock.

Para implementar a chamada a um serviço externo, houve a preocupação de garantir que o mecanismo de consulta fizesse uso de um pool de conexões HTTP e que esse pool fosse configurável externamente.

O pool foi configurado para aceitar 100 conexões simultâneas por rota, obdecendo o máximo global de 1000 conexões. Também é possível configurar os seguintes timeouts:

* connectionRequestTimeout: tempo máximo de espera que um cliente do connection pool está disposto a esperar por uma conexão.
* connectTimeout: tempo máximo de espera até o serviço externo aceitar a conexão
* socketTimeout: tempo máximo sem tráfego entre cliente e servidor uma vez aberta a conexão.
* idleConnectionTimeout: tempo máximo em que uma conexão ociosa antes de ser encerrada pelo connection manager.

```
http-pool:
   max-per-route: 100
   max-total: 1000
   connection-request-timeout-millis: 300
   connect-timeout-millis: 300
   socket-timeout-millis: 1500
   idle-connection-timeout-millis: 30000
```

Além disso, foi criado uma task assíncrona que periodicamente busca por conexões ociosas e as encerra, garantindo uso racional dos recursos.

### Exceções especialistas

Outra preocupação foi em garantir que os tipos das exceções deixassem claro qual o problema ocorrido. Algumas exceções que criei para este projeto:

* AssociateAlreadyVotedException
* AssociateUnableToVoteException
* UnableToAssertAssociateEligibilityException
* VotingSessionAlreadyStartedException
* VotingSessionClosedException
* VotingSessionNotFoundException
* VotingSessionNotStartedException

Essas exceções possuem atributos que além de enriquecerem a descrição do problema, ainda viabilizam testes mais objetivos.

### Logging

Além das mensagens de log em pontos importantes dos diversos fluxos, houve um cuidado de garantir tracing de requisições, o que é importante em um ambiente de múltiplas requisições simultâneas.

Utilizei o Spring Sleuth por ele se basear no OpenTracing e automaticamente repassar os trace ids para outras plataformas, como brokers de mensageria (no caso específico do Kafka, dada a versão do broker que utilizei, incompatível, tive que desligar a integração).

Como o Sleuth se integra naturalmente ao [ZipKin](https://zipkin.io/), poderíamos ter dashboards e análises geradas sobre os trace ids.

Os níveis de logs de algums frameworks, como Liquibase, Hibernate e Spring foram configurados para nível WARN.

### Tasks Assíncronas

No projeto temos duas, anteriormente mencionadas:

* publicador de resultados
* monitor de conexões ociosas

```
tasks:
   voting-results-publish:
      delay-millis: 30000
   idle-connections-monitor:
      delay-millis: 5000
```

### Docker e Docker Compose

O projeto possui dependências com broker de mensagens (Kafka) e banco de de dados (PostgreSQL). Para facilitar tanto o desenvolvimento quanto a execução, o projeto dispõe de um `docker-compose.yml` que sobe quatro serviços:

* zookeeper
* kafka
* database
* app

A porta 8080 da máquina host é direcionada para a porta 8080 do serviço app, onde está a aplicação (assumindo que o pacote tenha sido criado previamente).

### Sobre versionamento de API

Existem algumas abordagens relacionadas a versionamento de APIs:

* recurso completamente novo: /votacaoV1 e /votacaoV2
* baseado em path: /v1/votacao e /v2/votacao
* dominio separado: v1.api/votacao e v2.api/votacao
* mesma URI mas utilizando header `Accept` com Vendor Mime Type específico:
    * `application/vnd.sicredi.votacao-v1+json`
    * `application/vnd.sicredi.votacao-v2+json`

A abordagem baseada em path me parece mais adequada quando temos uma API que sofre evoluções e que não podemos simplesmente forçar os clientes a migrarem para a nova versão. É uma abordagem simples e clara e me parece um bom meio termo entre dar tempo para os clientes se adaptarem à nova versão e o custo proibitivo de se manter várias versões de API indefinidamente.

Se estamos falando de versões de API que não fruto de evoluções mas de diferentes representações, a abordagem de Mime Type parece mais interessante. Ela inclusive é mais aderente ao conceito do REST pois separa a identificação do recurso de sua representação.

### Como executar o projeto

Para execução do projeto assume-se que **docker**, **docker-compose**, **maven** e **java 8+** estejam instalados

1. clone o repositório:  
	`git clone https://neibarbosa@bitbucket.org/neibarbosa/voting-session-service.git`

2. execute o build do projeto:  
	`./mvnw clean install`

3. execute o compose:  
	`docker-compose up -d`

### Ferramentas de qualidade de código e cobertura

O projeto foi analisado localmente com SonarQube (container com servidor local) e Clover. Para gerar os reports de ambas as ferramentas

Para subir o container do SonarQube:

```docker run -d --name sonarqube -p 9000:9000 sonarqube```

Para gerar os relatórios:

```
./mvnw clean \
org.openclover:clover-maven-plugin:setup \
test \
org.openclover:clover-maven-plugin:aggregate \
org.openclover:clover-maven-plugin:clover \
```

O ponto de entrada do relatórios do Clover fica em `target/site/clover/index.html`

```
./mvnw clean package sonar:sonar
```

Por algum motivo a cobertura não estava sendo propagada para o Sonar. Por isso os comandos separados. Oportunamente resolverei esse ponto.