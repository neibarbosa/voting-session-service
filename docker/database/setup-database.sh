#!/bin/bash

set -e

psql -U postgres -tc "SELECT 1 FROM pg_user WHERE usename = '$DB_USER'" | grep -q 1 || psql -U postgres -c "CREATE USER $DB_USER PASSWORD '$DB_PASS';"
echo "****** USER [$DB_USER] CREATED******"

psql -U postgres -tc "SELECT 1 FROM pg_database WHERE datname = '$DB_NAME';" | grep -q 1 || psql -U postgres -c "CREATE DATABASE $DB_NAME WITH OWNER $DB_USER TEMPLATE template0 ENCODING 'UTF8';"
echo "****** DATABASE [$DB_NAME] CREATED******"

psql -U postgres -tc "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO $DB_USER;"
echo "****** PRIVILEGES GRANTED ON DATABASE $DB_NAME TO $DB_USER ******"
