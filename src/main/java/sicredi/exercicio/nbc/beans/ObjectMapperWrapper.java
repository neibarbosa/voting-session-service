package sicredi.exercicio.nbc.beans;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import sicredi.exercicio.nbc.exception.RuntimeJsonProcessingException;

/**
 * This class wraps an {@link ObjectMapper} instance and delegate to its methods
 * throwing a {@link RuntimeJsonProcessingException} instead of
 * {@link JsonProcessingException} which is checked.<br>
 * 
 * Not all methods of the wrapped instance have delegates. They should be added
 * as they are needed.
 * 
 * @author nei
 *
 */
public class ObjectMapperWrapper {

	private ObjectMapper mapper;

	public ObjectMapperWrapper(ObjectMapper mapper) {
		this.mapper = mapper;
	}

	public String writeValueAsString(Object value) {
		try {
			return mapper.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			throw new RuntimeJsonProcessingException(e);
		}
	}

}
