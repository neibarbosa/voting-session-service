package sicredi.exercicio.nbc.config;

import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import sicredi.exercicio.nbc.beans.ObjectMapperWrapper;

@Getter
@Configuration
public class AppConfig {

	@Value("${voting.results.topic:voting-results}")
	private String votingResultsTopic;

	@Bean
	public MessageSource messageSource() {
		final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding(StandardCharsets.UTF_8.displayName());
		return messageSource;
	}

	@Bean
	public LocalValidatorFactoryBean getValidator() {
	    LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
	    bean.setValidationMessageSource(messageSource());
	    return bean;
	}

	@Bean
	public ObjectMapperWrapper objectMapperWrapper(ObjectMapper mapper) {
	    return new ObjectMapperWrapper(mapper);
	}

}
