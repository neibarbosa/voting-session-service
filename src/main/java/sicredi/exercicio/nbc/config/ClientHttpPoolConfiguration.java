package sicredi.exercicio.nbc.config;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * Configuration class that builds the beans that will enable the use of a connection pool.
 * 
 * @author nei
 *
 */
@Configuration
public class ClientHttpPoolConfiguration {

	@Autowired
	private HttpConnectionPoolConfiguration poolConfiguration;

	@Bean
	public PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
		PoolingHttpClientConnectionManager result = new PoolingHttpClientConnectionManager();
		result.setDefaultMaxPerRoute(poolConfiguration.getMaxPerRoute());
		result.setMaxTotal(poolConfiguration.getMaxTotal());
		return result;
	}

	@Bean
	public RequestConfig requestConfig() {
		return RequestConfig.custom()
				.setConnectionRequestTimeout(poolConfiguration.getConnectionRequestTimeoutMillis())
				.setConnectTimeout(poolConfiguration.getConnectTimeoutMillis())
				.setSocketTimeout(poolConfiguration.getSocketTimeoutMillis()).build();
	}

	/**
	 * Builds an {@link HttpClient} that will be used in {@link RestTemplate}.
	 * <p>
	 * It uses a keep alive strategy and a configurable default request configuration.
	 * 
	 * @param poolingHttpClientConnectionManager
	 * @param requestConfig
	 * @return
	 * 
	 * @see RestTemplateConfiguration
	 */
	@Bean
	public CloseableHttpClient httpClient(PoolingHttpClientConnectionManager poolingHttpClientConnectionManager,
			RequestConfig requestConfig) {
		return HttpClientBuilder.create()
				.setKeepAliveStrategy(keepAliveStrategy())
				.setConnectionManager(poolingHttpClientConnectionManager)
				.setDefaultRequestConfig(requestConfig).build();
	}

	/**
	 * 
	 * @return a keep alive strategy that honors header Keep-Alive if present but that defaults
	 * to 5 seconds if not.
	 * 
	 * @see <a href="https://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d5e425">Keep Alive strategy</a>
	 */
	private ConnectionKeepAliveStrategy keepAliveStrategy() {
		return (response, context) -> {
				HeaderElementIterator it = new BasicHeaderElementIterator(
						response.headerIterator(HTTP.CONN_KEEP_ALIVE));
				while (it.hasNext()) {
					HeaderElement he = it.nextElement();
					String param = he.getName();
					String value = he.getValue();
					if (value != null && param.equalsIgnoreCase("timeout")) {
						return Long.parseLong(value) * 1000;
					}
				}
				return 5 * 1000;
		};
	}
}