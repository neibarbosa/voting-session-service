package sicredi.exercicio.nbc.config;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import sicredi.exercicio.nbc.tasks.IdleHttpConnectionsMonitor;

/**
 * Configuration class to hold data used to create an HTTP connection pool.
 * <p>
 * 
 * @author nei
 * 
 * @see RequestConfig
 * @see PoolingHttpClientConnectionManager
 *
 */
@Component
@ConfigurationProperties(prefix = "http-pool")
@Getter
@Setter
@ToString
public class HttpConnectionPoolConfiguration {

	/**
	 * Default maximum number of connections per http route
	 * 
	 * @see PoolingHttpClientConnectionManager
	 */
	private Integer maxPerRoute = 2;

	/**
	 * Global maximum number of connections
	 * 
	 * @see PoolingHttpClientConnectionManager
	 */
	private Integer maxTotal = 20;

    /**
     * @see RequestConfig#getConnectionRequestTimeout()
     */
    private Integer connectionRequestTimeoutMillis = -1;
    
    /**
     * @see RequestConfig#getConnectTimeout()
     */
    private Integer connectTimeoutMillis = -1;
    
    /**
     * @see RequestConfig#getSocketTimeout()
     */
    private Integer socketTimeoutMillis = -1;
    
    /**
     * Maximum time in millis an idle connection will be kept before being closed.
     * 
     * @see IdleHttpConnectionsMonitor
     */
    private Integer idleConnectionTimeoutMillis = 30000;

}