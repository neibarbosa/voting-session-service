package sicredi.exercicio.nbc.config;

import org.apache.http.client.HttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@Configuration
@Import({ HttpConnectionPoolConfiguration.class, ClientHttpPoolConfiguration.class })
public class RestTemplateConfiguration {

	/**
	 * Builds a {@link RestTemplate} instance with an injected {@link HttpClient} bean.
	 * <p>
	 * It is assumed the {@link HttpClient} bean uses a connection pool.
	 * 
	 * @param httpClient 
	 * @return 
	 * 
	 * @see ClientHttpPoolConfiguration 
	 */
	@Bean
	public RestTemplate restTemplate(HttpClient httpClient) {
		HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
		httpRequestFactory.setHttpClient(httpClient);
		return new RestTemplate(httpRequestFactory);
	}
}
