package sicredi.exercicio.nbc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import sicredi.exercicio.nbc.VotingSessionApplication;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {                                    

	@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage(VotingSessionApplication.class.getPackage().getName()))              
          .paths(PathSelectors.any())
          .build()
          .tags(new Tag("voting-session", "APIs for managing and participating in voting sessions"))
          .apiInfo(apiInfo());
    }

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("Voting Session API")
				.build();
	}
}