package sicredi.exercicio.nbc.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import sicredi.exercicio.nbc.controller.dto.CreateNewVotingSessionRequestDTO;
import sicredi.exercicio.nbc.controller.dto.CreateNewVotingSessionResponseDTO;
import sicredi.exercicio.nbc.controller.dto.VoteRequestDTO;
import sicredi.exercicio.nbc.controller.dto.VoteResponseDTO;
import sicredi.exercicio.nbc.controller.dto.VotingSessionDTO;
import sicredi.exercicio.nbc.controller.validator.DurationConstraint;
import sicredi.exercicio.nbc.controller.validator.SessionIdConstraint;
import sicredi.exercicio.nbc.domain.VotingSessionResult;
import sicredi.exercicio.nbc.domain.VotingSession;
import sicredi.exercicio.nbc.dto.VotingSessionResultDTO;
import sicredi.exercicio.nbc.dto.Vote;
import sicredi.exercicio.nbc.service.VotingSessionService;

@Slf4j
@Api(tags= {"voting-session"})
@RequestMapping("/voting-session")
@RestController
@Validated
public class VotingController {

	private VotingSessionService service;

	@Autowired
	public VotingController(VotingSessionService service) {
		this.service = service;
	}
	
	@ApiOperation(value = "Creates a new voting session not open for receiving votes.")
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public CreateNewVotingSessionResponseDTO createVotingSession(
			@ApiParam(name="create-new-voting-session-request", value="Model to create a new voting session")
			@Valid @RequestBody
			final CreateNewVotingSessionRequestDTO request) {
		log.info("Request to create a new voting session: {}", request);
		final VotingSession votingSession = service.createSession(new VotingSession(request.getAgenda()));
		return new CreateNewVotingSessionResponseDTO(votingSession.getId(), votingSession.getAgenda());
	}

	@ApiOperation(value = "Starts a voting session, enabling it to receive votes.")
	@PostMapping(value = "/{id}/start")
	public VotingSessionDTO startVotingSession(
			@ApiParam(name="id", value="Id of the voting session", example="42")
			@PathVariable("id") @SessionIdConstraint 
			final Integer sessionId,

			@ApiParam(name="durationInMinutes", value="Amount of time, in minutes, associates will be able to vote for a voting session", example="3", defaultValue="1")
			@RequestParam(name = "durationInMinutes", defaultValue = "1") @DurationConstraint 
			final Integer durationInMinutes
			) {
		log.info("Request to open voting session {} to receive votes for {} minutes", sessionId, durationInMinutes);
		final VotingSession session = service.startSession(sessionId, durationInMinutes);
		return toVotingSessionDTO(session);
	}

	private VotingSessionDTO toVotingSessionDTO(final VotingSession session) {
		final VotingSessionDTO response = new VotingSessionDTO();
		response.setId(session.getId());
		response.setAgenda(session.getAgenda());
		response.setStarting(session.getStarting());
		response.setClosing(session.getClosing());
		return response;
	}

	@ApiOperation(value = "Count votes, giving a partial result.")
	@GetMapping(value = "/{id}/result")
	public VotingSessionResultDTO countVotes(
			@ApiParam(name="id", value="Id of the voting session", example="42")
			@PathVariable("id") @SessionIdConstraint 
			final Integer sessionId) {
		log.info("Request to get current results of voting session {}", sessionId);
		final VotingSessionResult result = service.countVotes(sessionId);
		return VotingSessionResultDTO.from(result);
	}

	@ApiOperation(value = "Posts a vote for an option in a voting session.")
	@PostMapping("{id}/vote")
	public VoteResponseDTO vote(
			@ApiParam(name = "id", value="Id of the voting session", example="42")
			@PathVariable("id") @SessionIdConstraint
			final Integer sessionId, 
			@ApiParam(name = "vote-request", value="Data required to register the vote")
			@Valid @RequestBody
			final VoteRequestDTO voteDTO) {
		log.info("Request to register a vote in voting session {}: {}", sessionId, voteDTO);
		final Vote vote = voteDTO.asVote(sessionId);
		Vote registerVote = service.registerVote(vote);
		return new VoteResponseDTO(registerVote.getOption());
	}

}
