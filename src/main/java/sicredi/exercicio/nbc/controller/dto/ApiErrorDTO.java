package sicredi.exercicio.nbc.controller.dto;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApiErrorDTO {
	 
    private final HttpStatus status;
    private final List<String> errors;
 
    public ApiErrorDTO(HttpStatus status, List<String> errors) {
        this.status = status;
        this.errors = errors;
    }
 
    public ApiErrorDTO(HttpStatus status, String error) {
        this.status = status;
        errors = Arrays.asList(error);
    }
}