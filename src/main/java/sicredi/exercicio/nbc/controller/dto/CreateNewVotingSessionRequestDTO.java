package sicredi.exercicio.nbc.controller.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ApiModel(value = "create-new-voting-session-request", description = "Data required to create a new voting session")
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class CreateNewVotingSessionRequestDTO {

	@ApiModelProperty(name = "agenda", value = "The issue to be voted in the voting session")
	@NotBlank(message = "{votingsession.agenda.nonblank}")
	private String agenda;

}
