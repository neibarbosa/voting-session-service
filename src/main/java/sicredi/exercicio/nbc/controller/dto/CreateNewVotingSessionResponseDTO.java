package sicredi.exercicio.nbc.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;


@ApiModel(value = "create-new-voting-session-response", description = "Representation of a voting session just created")
@Getter
@Setter
@RequiredArgsConstructor
public class CreateNewVotingSessionResponseDTO {

	@ApiModelProperty(name = "id", value = "Id of the votiteteteng session", example="42")
	private final Integer id;
	
	@ApiModelProperty(value = "The issue to be voted in the voting session")
	private final String agenda;

}
