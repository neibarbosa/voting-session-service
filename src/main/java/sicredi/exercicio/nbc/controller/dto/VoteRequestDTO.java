package sicredi.exercicio.nbc.controller.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import sicredi.exercicio.nbc.dto.Vote;

@ApiModel(value = "vote-request", description = "Represents an option an associate has chosen in a voting session")
@Getter
@Setter
@RequiredArgsConstructor
@ToString
public class VoteRequestDTO {

	@NotBlank(message = "{vote.associateid.invalid}")
	@Size(max = 11, message = "{vote.associateid.max.exceeded}")
	@JsonProperty("associate-id")
	private final String associateId;

	@ApiModelProperty(allowableValues="yes,no")
	@NotBlank(message = "{vote.option.notempty}")
	@Pattern(regexp = "yes|no", flags = Pattern.Flag.CASE_INSENSITIVE, message = "{vote.option.invalid}")
	private final String option;

	public Vote asVote(int sessionId) {
		return new Vote(sessionId, associateId, option);
	}
	
}
