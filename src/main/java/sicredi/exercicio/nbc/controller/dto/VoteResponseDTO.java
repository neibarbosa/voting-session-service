package sicredi.exercicio.nbc.controller.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@ApiModel(value = "vote-response", description = "Response returned after a vote is registered")
@Getter
@Setter
@RequiredArgsConstructor
public class VoteResponseDTO {

	private final String option;
}
