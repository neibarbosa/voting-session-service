package sicredi.exercicio.nbc.controller.dto;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "voting-session", description = "Representation of a voting session")
@Getter
@Setter
public class VotingSessionDTO {

	@ApiModelProperty(value = "Id of the voting session")
	private Integer id;
	
	@ApiModelProperty(value = "The issue to be voted in the voting session")
	private String agenda;

	@ApiModelProperty(value = "Starting time of the voting session according to America/São Paulo timezone (yyyy-MM-dd'T'HH:mm:ss.SSS)")
	private LocalDateTime starting;

	@ApiModelProperty(value = "Closing time of the voting session according to America/São Paulo timezone (yyyy-MM-dd'T'HH:mm:ss.SSS)")
	private LocalDateTime closing;
}
