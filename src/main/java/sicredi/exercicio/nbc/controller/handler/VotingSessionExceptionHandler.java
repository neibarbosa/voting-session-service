package sicredi.exercicio.nbc.controller.handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import lombok.extern.slf4j.Slf4j;
import sicredi.exercicio.nbc.controller.dto.ApiErrorDTO;
import sicredi.exercicio.nbc.exception.AssociateAlreadyVotedException;
import sicredi.exercicio.nbc.exception.AssociateUnableToVoteException;
import sicredi.exercicio.nbc.exception.UnableToAssertAssociateEligibilityException;
import sicredi.exercicio.nbc.exception.VotingSessionAlreadyStartedException;
import sicredi.exercicio.nbc.exception.VotingSessionClosedException;
import sicredi.exercicio.nbc.exception.VotingSessionNotFoundException;
import sicredi.exercicio.nbc.exception.VotingSessionNotStartedException;

@Slf4j
@ControllerAdvice
public class VotingSessionExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		final List<String> errors = new ArrayList<>();

		final BindingResult bindingResult = ex.getBindingResult();

		for (FieldError error : bindingResult.getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}

		for (ObjectError error : bindingResult.getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}

		log.error("{}", errors);
		final ApiErrorDTO apiError = new ApiErrorDTO(HttpStatus.BAD_REQUEST, errors);
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(
			HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		Throwable cause = ex.getCause();
		if (cause.getClass().isAssignableFrom(JsonParseException.class)) {
			return handleCause(status, JsonParseException.class, cause, t -> t.getOriginalMessage());
		} else if (cause.getClass().isAssignableFrom(InvalidFormatException.class)) {
			return handleCause(status, InvalidFormatException.class, cause, t -> t.getOriginalMessage());
		}
		
		return handleExceptionInternal(ex, null, headers, status, request);
	}

	private <T extends Throwable> ResponseEntity<Object> handleCause(HttpStatus status, Class<T> clazz, Throwable cause, Function<T, String> f) {
		final T castException = clazz.cast(cause);
		final String message = f.apply(castException);
		log.error(message);
		final ApiErrorDTO apiError = new ApiErrorDTO(status, message);
		return new ResponseEntity<>(apiError, status);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex, WebRequest request) {

		final HttpHeaders headers = new HttpHeaders();
		final Set<ConstraintViolation<?>> constraintViolations = ex.getConstraintViolations();

		final List<String> errors = constraintViolations.stream()
				.filter(Objects::nonNull)
				.map(cv -> StringUtils.substringAfterLast(String.valueOf(cv.getPropertyPath()), ".") + ": " + cv.getMessage())
				.collect(Collectors.toList());

		log.error("{}", errors);
		final ApiErrorDTO apiError = new ApiErrorDTO(HttpStatus.BAD_REQUEST, errors);
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest request) {

		final HttpHeaders headers = new HttpHeaders();
		Object offendingValue = ex.getValue();
		String message = ex.getName() + ": expected to be " + ex.getRequiredType().getSimpleName() + " but was " + offendingValue.getClass().getSimpleName();
		log.error(message);
		final ApiErrorDTO apiError = new ApiErrorDTO(HttpStatus.BAD_REQUEST, message);
		return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
	}


	@ExceptionHandler(VotingSessionAlreadyStartedException.class)
	public ResponseEntity<ApiErrorDTO> handleVotingSessionAlreadyStarted(VotingSessionAlreadyStartedException ex, WebRequest request) {
		return handleGeneric(HttpStatus.BAD_REQUEST, ex);
	}

	@ExceptionHandler(VotingSessionNotFoundException.class)
	public ResponseEntity<ApiErrorDTO> handleVotingSessionNotFound(VotingSessionNotFoundException ex, WebRequest request) {

		log.error(ex.getMessage());
		final HttpStatus status = HttpStatus.NOT_FOUND;
		final ApiErrorDTO apiError = new ApiErrorDTO(status, ex.getMessage());

		return new ResponseEntity<>(apiError, status);
	}

	@ExceptionHandler(VotingSessionClosedException.class)
	public ResponseEntity<ApiErrorDTO> handleVotingSessionClosed(VotingSessionClosedException ex, WebRequest request) {
		return handleGeneric(HttpStatus.BAD_REQUEST, ex);
	}

	@ExceptionHandler(VotingSessionNotStartedException.class)
	public ResponseEntity<ApiErrorDTO> handleVotingSessionNotStarted(VotingSessionNotStartedException ex, WebRequest request) {
		return handleGeneric(HttpStatus.BAD_REQUEST, ex);
	}

	@ExceptionHandler(AssociateAlreadyVotedException.class)
	public ResponseEntity<ApiErrorDTO> handleAssociateAlreadyVoted(AssociateAlreadyVotedException ex, WebRequest request) {
		return handleGeneric(HttpStatus.FORBIDDEN, ex);
	}

	@ExceptionHandler(AssociateUnableToVoteException.class)
	public ResponseEntity<ApiErrorDTO> handleAssociateUnableToVote(AssociateUnableToVoteException ex, WebRequest request) {
		return handleGeneric(HttpStatus.NOT_FOUND, ex);
	}
	
	@ExceptionHandler(UnableToAssertAssociateEligibilityException.class)
	public ResponseEntity<ApiErrorDTO> handleUnableToAssertAssociateEligibility(UnableToAssertAssociateEligibilityException ex, WebRequest request) {
		return handleGeneric(HttpStatus.INTERNAL_SERVER_ERROR, ex);
	}
	
	private ResponseEntity<ApiErrorDTO> handleGeneric(HttpStatus status, Exception ex) {
		log.error("exception: {} ### cause: {} ### message: {}", ClassUtils.getSimpleName(ex), ClassUtils.getSimpleName(ex.getCause()), ex.getMessage());
		final ApiErrorDTO apiError = new ApiErrorDTO(status, ex.getMessage());
		return new ResponseEntity<>(apiError, status);
	}

}