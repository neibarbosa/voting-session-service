package sicredi.exercicio.nbc.controller.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = SessionIdValidator.class)
@Target( { ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface SessionIdConstraint {
    String message() default "{votingsession.id.invalid}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}