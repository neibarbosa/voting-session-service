package sicredi.exercicio.nbc.controller.validator;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SessionIdValidator implements ConstraintValidator<SessionIdConstraint, Integer> {

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext context) {
		return Objects.nonNull(value) && value.intValue() > 0;
	}

}
