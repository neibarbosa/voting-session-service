package sicredi.exercicio.nbc.domain;

import static sicredi.exercicio.nbc.utils.VotingSessionConstants.BR_ZONE_ID;

import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "voting-session", description = "Data about a voting session")
@Getter
@Setter
@Entity
@Table(name = "voting_session")
public class VotingSession {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String agenda;
	
	@JsonIgnore
	@Version
	private Integer version; 

	private LocalDateTime starting;
	
	private LocalDateTime closing;

	@JsonIgnore
	private boolean published;
	
	protected VotingSession() {
	}

	public VotingSession(String agenda) {
		if (StringUtils.isBlank(agenda)) {
			throw new IllegalArgumentException("agenda cannot be blank");
		}
		this.agenda = agenda.trim();
	}

	@JsonIgnore
	public boolean isNotStarted() {
		return Objects.isNull(starting);
	}

	@JsonIgnore
	public boolean isClosed() {
		return Objects.nonNull(closing) && closing.isBefore(LocalDateTime.now(BR_ZONE_ID));
	}
}