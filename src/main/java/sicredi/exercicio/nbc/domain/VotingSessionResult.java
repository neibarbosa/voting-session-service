package sicredi.exercicio.nbc.domain;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class VotingSessionResult {

	private final VotingSession votingSession;
	private final Map<String, Long> totals = new HashMap<>();

	public Map<String, Long> getTotals() {
		return new HashMap<>(totals);
	}

	public long addResult(String option, long total) {
		long newTotal = totals.merge(option, total, Long::sum);
		long oldTotal = newTotal - total;
		log.trace("Added {} votes for option {} in voting session {} - prev = {}, cur = {}", total, option, votingSession.getId(), oldTotal, newTotal);
		return newTotal;
	}
}
