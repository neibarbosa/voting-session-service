package sicredi.exercicio.nbc.dto;

import lombok.Data;

@Data
public class Vote {

	private final int sessionId;
	private final String associateId;
	private final String option;
}
