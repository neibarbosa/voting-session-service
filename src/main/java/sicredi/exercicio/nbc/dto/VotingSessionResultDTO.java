package sicredi.exercicio.nbc.dto;

import java.util.Map;
import java.util.TreeMap;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.ToString;
import sicredi.exercicio.nbc.domain.VotingSessionResult;
import sicredi.exercicio.nbc.domain.VotingSession;


@ApiModel(value = "voting-session-result", description = "Data about the voting session and its current result")
@Getter
@ToString
public class VotingSessionResultDTO {

	@ApiModelProperty(name = "voting-session", value = "The voting session the result refers to")
	@JsonProperty("voting-session")
	private VotingSession votingSession;

	@ApiModelProperty(value = "The results by option. Only the options that received at least one vote are shown")
	private Map<String, Long> totals;
	
	public static VotingSessionResultDTO from(VotingSessionResult partialResult) {
		VotingSessionResultDTO result = new VotingSessionResultDTO();
		result.setVotingSession(partialResult.getVotingSession());
		result.setTotals(partialResult.getTotals());
		return result;
	}

	private void setTotals(Map<String, Long> totalsByOptions) {
		this.totals = new TreeMap<>(totalsByOptions);
	}

	private void setVotingSession(VotingSession votingSession) {
		this.votingSession = votingSession;
	}

}
