package sicredi.exercicio.nbc.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import sicredi.exercicio.nbc.domain.VotingSession;

@Getter
@Setter
@Entity
@Table(name = "vote")
public class VoteEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "voting_session_id")
	private VotingSession votingSession;
	
	@Column(name = "associate_id")
	private String associateId;

	private String option;

}