package sicredi.exercicio.nbc.exception;

import lombok.Getter;

@Getter
public class AssociateAlreadyVotedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final int sessionId;
	private final String associateId;

	public AssociateAlreadyVotedException(int sessionId, String associateId, Throwable cause) {
		super(String.format("Associate %s already has a vote registered in voting session %s", associateId, sessionId), cause);
		this.sessionId = sessionId;
		this.associateId = associateId;
	}
}
