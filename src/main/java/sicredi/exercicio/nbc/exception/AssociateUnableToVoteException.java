package sicredi.exercicio.nbc.exception;

import lombok.Getter;

@Getter
public class AssociateUnableToVoteException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String associateId;
	
	public AssociateUnableToVoteException(String associateId) {
		super(String.format("Associate %s is unable to vote", associateId));
		this.associateId = associateId;
	}

}
