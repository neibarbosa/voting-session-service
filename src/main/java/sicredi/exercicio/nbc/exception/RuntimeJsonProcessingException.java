package sicredi.exercicio.nbc.exception;

import com.fasterxml.jackson.core.JsonProcessingException;

public class RuntimeJsonProcessingException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RuntimeJsonProcessingException(JsonProcessingException e) {
		super(e);
	}
}
