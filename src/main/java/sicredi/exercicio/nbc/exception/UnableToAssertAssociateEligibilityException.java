package sicredi.exercicio.nbc.exception;

import lombok.Getter;

@Getter
public class UnableToAssertAssociateEligibilityException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final String associateId;
	
	public UnableToAssertAssociateEligibilityException(String associateId, Throwable e) {
		super(String.format("It was not possible to assert if associate %s is able or not to vote", associateId), e);
		this.associateId = associateId;
	}

}
