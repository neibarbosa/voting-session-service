package sicredi.exercicio.nbc.exception;

import lombok.Getter;
import sicredi.exercicio.nbc.domain.VotingSession;

@Getter
public class VotingSessionAlreadyStartedException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	private final VotingSession votingSession;
	
	public VotingSessionAlreadyStartedException(VotingSession votingSession) {
		super(String.format("Could not start voting session [%s] because it was started in %s", votingSession.getId(), votingSession.getStarting()));
		this.votingSession = votingSession;
	}

}
