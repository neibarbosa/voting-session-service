package sicredi.exercicio.nbc.exception;

import lombok.Getter;

@Getter
public class VotingSessionClosedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final int id;

	public VotingSessionClosedException(int id) {
		super(String.format("Voting session %s is closed", id));
		this.id = id;
	}
	
}
