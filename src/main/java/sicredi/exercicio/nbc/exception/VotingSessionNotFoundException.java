package sicredi.exercicio.nbc.exception;

import lombok.Getter;

@Getter
public class VotingSessionNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final int id;
	
	public VotingSessionNotFoundException(int id) {
		super(String.format("Voting session [%s] could not be found", id));
		this.id = id;
	}

}
