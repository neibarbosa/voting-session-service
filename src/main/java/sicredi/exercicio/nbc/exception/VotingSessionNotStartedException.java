package sicredi.exercicio.nbc.exception;

import lombok.Getter;

@Getter
public class VotingSessionNotStartedException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final int id;

	public VotingSessionNotStartedException(int id) {
		super(String.format("Voting session %s was not started", id));
		this.id = id;
	}
}
