package sicredi.exercicio.nbc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sicredi.exercicio.nbc.entity.VoteEntity;

@Repository
public interface VoteRepository extends JpaRepository<VoteEntity, Integer> {

	List<VoteEntity> findByVotingSessionId(Integer sessionId);

}
