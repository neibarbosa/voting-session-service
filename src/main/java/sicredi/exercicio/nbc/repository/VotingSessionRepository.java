package sicredi.exercicio.nbc.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sicredi.exercicio.nbc.domain.VotingSession;

@Repository
public interface VotingSessionRepository extends JpaRepository<VotingSession, Integer> {

	List<VotingSession> findByClosingBeforeAndPublished(LocalDateTime now, boolean published);

}
