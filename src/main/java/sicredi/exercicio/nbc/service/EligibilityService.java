package sicredi.exercicio.nbc.service;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;
import sicredi.exercicio.nbc.exception.UnableToAssertAssociateEligibilityException;

@Slf4j
@Service
public class EligibilityService {

	private RestTemplate restTemplate;
	
	@Value("${voting.cpf-validator.url}")
	private String cpfValidatorUrl;

	@Autowired
	public EligibilityService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public boolean isEligible(String associateId) {
		URI uri = UriComponentsBuilder.fromUriString(cpfValidatorUrl).buildAndExpand(associateId).toUri();
		try {
			ResponseEntity<Void> response = restTemplate.getForEntity(uri, Void.class);
			return response.getStatusCode().is2xxSuccessful();
		} catch (HttpClientErrorException e) {
			log.info("Query of associate {} returned code {}", associateId, e.getStatusCode());
			return false;
		} catch (HttpServerErrorException e) {
			log.error("Could not assert if associate {} is eligible for voting. Return code was {}", associateId, e.getStatusCode());
			throw new UnableToAssertAssociateEligibilityException(associateId, e);
		} catch (Exception e) {
			log.error("Could not assert if associate {} is eligible for voting. {}", associateId, e.getMessage(), e);
			throw new UnableToAssertAssociateEligibilityException(associateId, e);
		}
	}
	

}
