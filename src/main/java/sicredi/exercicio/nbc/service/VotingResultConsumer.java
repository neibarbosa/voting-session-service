package sicredi.exercicio.nbc.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class VotingResultConsumer {

    private final Logger logger = LoggerFactory.getLogger(VotingResultConsumer.class);

    @KafkaListener(topics = "voting-results", groupId = "sicredi")
    public void consume(String message) {
        logger.info("#### -> Consumed message -> {}", message);
    }
}