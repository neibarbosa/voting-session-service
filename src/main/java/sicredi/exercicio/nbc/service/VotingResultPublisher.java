package sicredi.exercicio.nbc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import sicredi.exercicio.nbc.beans.ObjectMapperWrapper;
import sicredi.exercicio.nbc.domain.VotingSessionResult;

@Slf4j
@Service
public class VotingResultPublisher {

	private ObjectMapperWrapper objectMapper;
	private KafkaTemplate<String, String> kafkaTemplate;

	@Autowired
	public VotingResultPublisher(ObjectMapperWrapper objectMapper, KafkaTemplate<String, String> kafkaTemplate) {
		this.objectMapper = objectMapper;
		this.kafkaTemplate = kafkaTemplate;
	}

    public void sendVotingResult(VotingSessionResult result, String topic) {
    	String jsonString = objectMapper.writeValueAsString(result);
        log.info("Publishing result to message broker: {}", jsonString);
        this.kafkaTemplate.send(topic, jsonString);
    }
}