package sicredi.exercicio.nbc.service;

import static sicredi.exercicio.nbc.utils.VotingSessionConstants.BR_ZONE_ID;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import lombok.extern.slf4j.Slf4j;
import sicredi.exercicio.nbc.config.AppConfig;
import sicredi.exercicio.nbc.domain.VotingSessionResult;
import sicredi.exercicio.nbc.domain.VotingSession;
import sicredi.exercicio.nbc.dto.Vote;
import sicredi.exercicio.nbc.entity.VoteEntity;
import sicredi.exercicio.nbc.exception.AssociateAlreadyVotedException;
import sicredi.exercicio.nbc.exception.AssociateUnableToVoteException;
import sicredi.exercicio.nbc.exception.VotingSessionAlreadyStartedException;
import sicredi.exercicio.nbc.exception.VotingSessionClosedException;
import sicredi.exercicio.nbc.exception.VotingSessionNotFoundException;
import sicredi.exercicio.nbc.exception.VotingSessionNotStartedException;
import sicredi.exercicio.nbc.repository.VoteRepository;
import sicredi.exercicio.nbc.repository.VotingSessionRepository;

@Slf4j
@Service
public class VotingSessionService {

	private VotingSessionRepository votingSessionRepository;
	private VoteRepository voteRepository;
	private VotingResultPublisher publisher;
	private EligibilityService eligibilityService;
	private AppConfig appConfig;
	
	private static final int VOTING_SESSION_MINIMUM_DURATION = 1;

	@Autowired
	public VotingSessionService(
			VotingSessionRepository repository, 
			VoteRepository voteRepository, 
			VotingResultPublisher publisher, 
			EligibilityService eligibilityService, 
			AppConfig appConfig) {
		this.votingSessionRepository = repository;
		this.voteRepository = voteRepository;
		this.publisher = publisher;
		this.eligibilityService = eligibilityService;
		this.appConfig = appConfig;
	}

	public VotingSession createSession(VotingSession votingSession) {
		final VotingSession unsaved = new VotingSession(votingSession.getAgenda());
		final VotingSession saved = votingSessionRepository.save(unsaved);
		log.info("Created new voting session {} with agenda: {}", saved.getId(), saved.getAgenda());
		return saved;
	}

	public VotingSession startSession(int sessionId, int durationInMinutes) {

		assertMinimumDuration(durationInMinutes);
		
		final VotingSession session = votingSessionRepository.findById(sessionId).orElseThrow(() -> new VotingSessionNotFoundException(sessionId));
		
		if (Objects.nonNull(session.getStarting())) {
			log.error("Voting Session {} was already started at {}", session.getId(), session.getStarting());
			throw new VotingSessionAlreadyStartedException(session);
		}
		
		final LocalDateTime starting = now();
		final LocalDateTime closing = starting.plusMinutes(durationInMinutes);
		session.setStarting(starting);
		session.setClosing(closing);
		VotingSession savedVotingSession = votingSessionRepository.save(session);
		log.info("Voting session {} was started at {} and it will be open to receive votes until {}", savedVotingSession.getId(), savedVotingSession.getStarting(), savedVotingSession.getClosing());
		return savedVotingSession;
	}

	private void assertMinimumDuration(int durationInMinutes) {
		Assert.isTrue(durationInMinutes >= VOTING_SESSION_MINIMUM_DURATION, 
				() -> "duration must be greater than " + VOTING_SESSION_MINIMUM_DURATION + " minute(s)");
	}

	private LocalDateTime now() {
		return LocalDateTime.now(BR_ZONE_ID);
	}

	public Vote registerVote(Vote vote) {
		final int sessionId = vote.getSessionId();
		final VotingSession session = votingSessionRepository.findById(sessionId).orElseThrow(() -> new VotingSessionNotFoundException(sessionId));

		checkSessionIsStarted(session);
		checkSessionIsNotClosed(session);
		
		if (associateUnableToVote(vote.getAssociateId())) {
			log.info("Associate {} is not eligible for voting.", vote.getAssociateId());
			throw new AssociateUnableToVoteException(vote.getAssociateId());
		}
		
		final VoteEntity entity = toEntity(vote, session);
		
		try {
			log.info("Registering vote of associate {} in voting session {} ...", vote.getAssociateId(), sessionId);
			VoteEntity savedEntity = voteRepository.save(entity);
			Vote result = toDomain(savedEntity);
			log.info("Vote of associate {} in voting session {} registered", vote.getAssociateId(), sessionId);
			return result;
		} catch (DataIntegrityViolationException e) {
			log.error("Associate {} had already voted in voting session {}", vote.getAssociateId(), sessionId);
			throw new AssociateAlreadyVotedException(vote.getSessionId(), vote.getAssociateId(), e);
		}
	}

	private boolean associateUnableToVote(String associateId) {
		boolean isAbleToVote = eligibilityService.isEligible(associateId);
		return !isAbleToVote;
	}

	private Vote toDomain(VoteEntity savedEntity) {
		return new Vote(savedEntity.getVotingSession().getId(), savedEntity.getAssociateId(), savedEntity.getOption());
	}

	private VoteEntity toEntity(Vote vote, final VotingSession session) {
		final VoteEntity entity = new VoteEntity();
		entity.setAssociateId(vote.getAssociateId());
		entity.setVotingSession(session);
		entity.setOption(vote.getOption());
		return entity;
	}

	private void checkSessionIsNotClosed(final VotingSession session) {
		if (session.isClosed()) {
			log.error("Voting Session {} was closed to received votes at {}", session.getId(), session.getClosing());
			throw new VotingSessionClosedException(session.getId());
		}
	}

	private void checkSessionIsStarted(final VotingSession session) {
		if (session.isNotStarted()) {
			log.error("Voting Session {} is not open to received votes", session.getId());
			throw new VotingSessionNotStartedException(session.getId());
		}
	}

	public VotingSessionResult countVotes(Integer sessionId) {
		final VotingSession votingSession = votingSessionRepository.findById(sessionId).orElseThrow(() -> new VotingSessionNotFoundException(sessionId));
		return buildPartialResultForVotingSessionId(votingSession);
	}

	private VotingSessionResult buildPartialResultForVotingSessionId(final VotingSession votingSession) {
		final List<VoteEntity> voteEntities = voteRepository.findByVotingSessionId(votingSession.getId());
		log.info("Votes of session {} were retrieved for building results", votingSession.getId());
		final VotingSessionResult partialResult = new VotingSessionResult(votingSession);
		for (VoteEntity voteEntity : voteEntities) {
			partialResult.addResult(voteEntity.getOption(), 1);
		}
		log.info("Results of session {} were built", votingSession.getId());
		return partialResult;
	}

	public void publishResults() {
		log.info("Looking for finished voting sessions to publish their results");
		final List<VotingSession> closedVotingSessions = votingSessionRepository.findByClosingBeforeAndPublished(now(), false);
		
		if (closedVotingSessions.isEmpty()) {
			log.info("There are no new results to publish");
		}
		
		for (VotingSession votingSession : closedVotingSessions) {
			VotingSessionResult result = buildPartialResultForVotingSessionId(votingSession);
			log.info("Publishing result of voting session {} - {}", votingSession.getId(), votingSession.getAgenda());
			publisher.sendVotingResult(result, appConfig.getVotingResultsTopic());
			votingSession.setPublished(true);
			votingSessionRepository.save(votingSession);
		}
	}
}
