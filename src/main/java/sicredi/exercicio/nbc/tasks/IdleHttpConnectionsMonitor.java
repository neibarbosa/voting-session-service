package sicredi.exercicio.nbc.tasks;

import java.util.concurrent.TimeUnit;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import sicredi.exercicio.nbc.config.HttpConnectionPoolConfiguration;

@Slf4j
@Component
public class IdleHttpConnectionsMonitor {

	@Autowired
	private PoolingHttpClientConnectionManager manager;
	
	@Autowired
	private HttpConnectionPoolConfiguration config;
	
	@Scheduled(fixedDelayString="${tasks.idle-connections-monitor.delay-millis:3000}")
	public void closeExpiredAndIdleConnections() {
		manager.closeExpiredConnections();
        manager.closeIdleConnections(config.getIdleConnectionTimeoutMillis(), TimeUnit.MILLISECONDS);
        log.debug("Closed idle and expired connections");
    }
}
