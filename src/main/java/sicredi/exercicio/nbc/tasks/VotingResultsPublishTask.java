package sicredi.exercicio.nbc.tasks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import sicredi.exercicio.nbc.service.VotingSessionService;

@Slf4j
@Component
public class VotingResultsPublishTask {

	@Autowired
	private VotingSessionService votingSessionService;
	
	/**
	 * Publishes the results of closed voting sessions that were yet not published.
	 */
	@Scheduled(fixedDelayString="${tasks.voting-results-publish.delay-millis:30000}")
	public void publishVotingSessionResults() {
        log.debug("Checking if there are voting results to publish");
		votingSessionService.publishResults();
	}
}
