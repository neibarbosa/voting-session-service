package sicredi.exercicio.nbc.utils;

import java.time.ZoneId;

public final class VotingSessionConstants {

	public static final ZoneId BR_ZONE_ID = ZoneId.of("America/Sao_Paulo");
	
	private VotingSessionConstants() {
		// prevents instantiation
	}
}
