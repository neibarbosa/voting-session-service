package sicredi.exercicio.nbc;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.rule.EmbeddedKafkaRule;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VotingSessionApplicationTests {

	@ClassRule
    public static EmbeddedKafkaRule broker = new EmbeddedKafkaRule(1, false, "someTopic");

    @BeforeClass
    public static void setup() {
        String brokersAsString = broker.getEmbeddedKafka().getBrokersAsString();
		System.setProperty("spring.kafka.bootstrap-servers", brokersAsString);
    }
    
    @Test
	public void contextLoads() {
	}

}
