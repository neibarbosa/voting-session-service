package sicredi.exercicio.nbc.beans;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import sicredi.exercicio.nbc.exception.RuntimeJsonProcessingException;

public class ObjectMapperWrapperTest {

	@Test
	public void testWriteValueAsStringIsDelegatedToUnderlyingObjectMapper() throws JsonProcessingException {
		final ObjectMapper mockMapper = mock(ObjectMapper.class);
		final ObjectMapperWrapper wrapper = new ObjectMapperWrapper(mockMapper);
		
		final Object object = new Object();
		wrapper.writeValueAsString(object);
		
		verify(mockMapper).writeValueAsString(object);
	}

	@Test
	public void testWriteValueAsStringThrowsRuntimeJsonProcessingException() throws JsonProcessingException {
		final ObjectMapper mockMapper = mock(ObjectMapper.class);
		final JsonProcessingException mockException = mock(JsonProcessingException.class);
		when(mockMapper.writeValueAsString(any())).thenThrow(mockException);
		final ObjectMapperWrapper wrapper = new ObjectMapperWrapper(mockMapper);
		
		assertThatThrownBy(() -> wrapper.writeValueAsString(new Object()))
			.isInstanceOf(RuntimeJsonProcessingException.class)
			.hasCause(mockException);
	}

}
