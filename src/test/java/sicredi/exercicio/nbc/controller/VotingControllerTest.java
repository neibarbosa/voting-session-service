package sicredi.exercicio.nbc.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static sicredi.exercicio.nbc.test.ControllerUtils.badRequestResponse;
import static sicredi.exercicio.nbc.test.ControllerUtils.notFoundResponse;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import sicredi.exercicio.nbc.domain.VotingSession;
import sicredi.exercicio.nbc.domain.VotingSessionResult;
import sicredi.exercicio.nbc.dto.Vote;
import sicredi.exercicio.nbc.exception.AssociateAlreadyVotedException;
import sicredi.exercicio.nbc.exception.AssociateUnableToVoteException;
import sicredi.exercicio.nbc.exception.VotingSessionAlreadyStartedException;
import sicredi.exercicio.nbc.exception.VotingSessionClosedException;
import sicredi.exercicio.nbc.exception.VotingSessionNotFoundException;
import sicredi.exercicio.nbc.exception.VotingSessionNotStartedException;
import sicredi.exercicio.nbc.service.VotingSessionService;
import sicredi.exercicio.nbc.test.ControllerUtils;

@RunWith(SpringRunner.class)
@WebMvcTest(VotingController.class)
public class VotingControllerTest {


	private static final String VOTING_SESSION_BASE_URI = "/voting-session";
	private static final String VOTING_SESSION_BASE_URI_WITH_ID_TEMPLATE = VOTING_SESSION_BASE_URI + "/%s";
	private static final String VOTING_SESSION_RESULT_URI_TEMPLATE = VOTING_SESSION_BASE_URI_WITH_ID_TEMPLATE + "/result";
	private static final String START_VOTING_SESSION_URI_TEMPLATE = VOTING_SESSION_BASE_URI_WITH_ID_TEMPLATE + "/start";
	private static final String VOTE_URI_TEMPLATE = VOTING_SESSION_BASE_URI_WITH_ID_TEMPLATE + "/vote";

	private static final int DEFAULT_VOTING_SESSION_ID = 1;
	private static final String DEFAULT_ASSOCIATE_ID = "associate";
	private static final String DEFAULT_AGENDA = "Agenda";
	private static final LocalDateTime DEFAULT_STARTING = LocalDateTime.of(2019, 2, 28, 13, 01, 22);
	private static final LocalDateTime DEFAULT_CLOSING = DEFAULT_STARTING.plusMinutes(1);                                                            
	
	// JSON field names
	private static final String ID_FIELD = "id";
	private static final String AGENDA_FIELD = "agenda";
	private static final String STARTING_FIELD = "starting";
	private static final String CLOSING_FIELD = "closing";
	private static final String ASSOCIATE_ID_FIELD = "associate-id";
	private static final String OPTION_FIELD = "option";
	private static final String VOTING_SESSION_FIELD = "voting-session";
	private static final String TOTALS_FIELD = "totals";

	private static final String YES = "yes";
	private static final String NO = "no";
	private static final int DEFAULT_MINIMUM_DURATION = 1;

	@Autowired
    private MockMvc mockMvc;

    @MockBean
    private VotingSessionService service;

    @Before
    public void setUp() throws Exception {
    	VotingSession voting = defaultVotingSession();
		when(service.createSession(any(VotingSession.class))).thenReturn(voting);
		when(service.startSession(anyInt(), anyInt())).thenReturn(voting);
		when(service.registerVote(any())).then(inv -> inv.getArgument(0));
    }

	private VotingSession defaultVotingSession() {
		VotingSession voting = new VotingSession(DEFAULT_AGENDA);
    	voting.setId(DEFAULT_VOTING_SESSION_ID);
		voting.setStarting(DEFAULT_STARTING);
		voting.setClosing(DEFAULT_CLOSING);
		return voting;
	}
    
	@Test
	public void shouldCreateNewVotingSchedule() throws Exception {

		this.mockMvc.perform(postContentToVotingSessionUri(newVotingSessionRequestDefaultBody()))
			.andExpect(status().isCreated())
			.andExpect(content().json(newVotingSessionResponseDefaultBody(), true));
	}

	private MockHttpServletRequestBuilder postContentToVotingSessionUri(String jsonContent) throws Exception {
		return basePostRequestTo(VOTING_SESSION_BASE_URI).content(jsonContent);
	}
	
	private MockHttpServletRequestBuilder basePostRequestTo(String uri) {
		return post(uri).characterEncoding(StandardCharsets.UTF_8.displayName()).contentType(MediaType.APPLICATION_JSON);
	}
	
	private String newVotingSessionRequestDefaultBody() throws JSONException {
		return newVotingSessionRequestBody(DEFAULT_AGENDA);
	}
	
	private String newVotingSessionRequestBody(String agenda) throws JSONException {
		JSONObject node = new JSONObject().put(AGENDA_FIELD, agenda);
		return node.toString(2);
	}
	
	private String newVotingSessionResponseDefaultBody() throws JSONException {
		return newVotingSessionResponseBody(DEFAULT_VOTING_SESSION_ID, DEFAULT_AGENDA);
	}

	private String newVotingSessionResponseBody(int id, String agenda) throws JSONException {
		JSONObject node = new JSONObject().
				put(ID_FIELD, id).
				put(AGENDA_FIELD, agenda);
		return node.toString(2);
	}

	@Test
	public void creationOfNewVotingSessionShouldReturnBadRequestWhenAgendaIsEmpty() throws Exception {

		final String emptyAgenda = newVotingSessionRequestBody(StringUtils.EMPTY);
		final ResultActions postAction = this.mockMvc.perform(postContentToVotingSessionUri(emptyAgenda));
		expectBadRequestWhenAgendaIsEmpty(postAction);
	}

	private void expectBadRequestWhenAgendaIsEmpty(ResultActions postAction) throws Exception {
		expectBadRequestWithMessage(postAction, "agenda: {votingsession.agenda.nonblank}");
	}

	private void expectBadRequestWithMessage(ResultActions action, String message) throws Exception {
		action
			.andExpect(status().isBadRequest())
			.andExpect(content().json(badRequestResponse(message), true));
	}

	@Test
	public void creationOfNewVotingSessionShouldReturnBadRequestWhenAgendaIsBlank() throws Exception {
		final String blankAgenda = newVotingSessionRequestBody(StringUtils.SPACE);
		final ResultActions postAction = this.mockMvc.perform(postContentToVotingSessionUri(blankAgenda));
		expectBadRequestWhenAgendaIsEmpty(postAction);
	}

	@Test
	public void creationOfNewVotingSessionShouldReturnBadRequestWhenAgendaIsMissing() throws Exception {
		final String missingAgenda = "{}";
		final ResultActions postAction = this.mockMvc.perform(postContentToVotingSessionUri(missingAgenda));
		expectBadRequestWhenAgendaIsEmpty(postAction);
	}

	@Test
	public void shouldstartVotingSession() throws Exception { 
		this.mockMvc.perform(startVotingSessionRequestToSessionIdWithDuration("1", "2"))
			.andExpect(status().isOk())
			.andExpect(content().json(votingSessionReturnDefaultBody(), true));
	}

	private MockHttpServletRequestBuilder startVotingSessionRequestToSessionIdWithDuration(String sessionId, String durationInMinutes) {
		return startVotingSessionRequestToSessionId(sessionId).param("durationInMinutes", durationInMinutes);
	}

	private MockHttpServletRequestBuilder startVotingSessionRequestToSessionId(String sessionId) {
		String startUri = String.format(START_VOTING_SESSION_URI_TEMPLATE, sessionId);
		return basePostRequestTo(startUri);
	}

	private String votingSessionReturnDefaultBody() throws JSONException {
		return defaultVotingSessionDTOJsonObject().toString(2);
	}
	
	private JSONObject defaultVotingSessionDTOJsonObject() throws JSONException {
		final JSONObject node = new JSONObject().
				put(ID_FIELD, DEFAULT_VOTING_SESSION_ID).
				put(AGENDA_FIELD, DEFAULT_AGENDA).
				put(STARTING_FIELD, DEFAULT_STARTING).
				put(CLOSING_FIELD, DEFAULT_CLOSING);
		return node;
	}
	
	@Test
	public void shouldstartVotingSessionWithDefaultDurationOfOneMinute() throws Exception { 
		this.mockMvc.perform(startVotingSessionRequestToSessionId("1"))
			.andExpect(status().isOk())
			.andExpect(content().json(votingSessionReturnDefaultBody(), true));
		final ArgumentCaptor<Integer> intCaptor = ArgumentCaptor.forClass(Integer.class);
		verify(service).startSession(anyInt(), intCaptor.capture());
		assertThat(intCaptor.getValue()).as("[check defaulting to minimum duration]").isEqualTo(DEFAULT_MINIMUM_DURATION);
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestWhenSessionIdIsNegative() throws Exception { 
		final MockHttpServletRequestBuilder startVotingSessionRequest = startVotingSessionRequestToSessionId("-1");
		ResultActions action = this.mockMvc.perform(startVotingSessionRequest);
		expectBadRequestWhenSessionIdIsInvalid(action);
	}

	private void expectBadRequestWhenSessionIdIsInvalid(ResultActions action) throws Exception {
		expectBadRequestWithMessage(action, "sessionId: {votingsession.id.invalid}");
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestWhenSessionIdIsZero() throws Exception { 
		final MockHttpServletRequestBuilder startVotingSessionRequest = startVotingSessionRequestToSessionId("0");
		ResultActions action = this.mockMvc.perform(startVotingSessionRequest);
		expectBadRequestWhenSessionIdIsInvalid(action);
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestWhenSessionIdIsNotANumber() throws Exception { 
		final MockHttpServletRequestBuilder startVotingSessionRequest = startVotingSessionRequestToSessionId("a");
		ResultActions action = this.mockMvc.perform(startVotingSessionRequest);
		expectBadRequestWithMessage(action, "id: expected to be Integer but was String");
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestWhenDurationIsNegative() throws Exception { 
		final MockHttpServletRequestBuilder startVotingSessionRequest = startVotingSessionRequestToSessionIdWithDuration("1", "-2");
		ResultActions action = this.mockMvc.perform(startVotingSessionRequest);
		expectBadRequestWhenDurationIsInvalid(action);
	}

	private void expectBadRequestWhenDurationIsInvalid(ResultActions action) throws Exception {
		action
			.andExpect(status().isBadRequest())
			.andExpect(content().json(badRequestResponse("durationInMinutes: {votingsession.duration.minimum}"), true));
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestWhenDurationIsZero() throws Exception { 
		final MockHttpServletRequestBuilder startVotingSessionRequest = startVotingSessionRequestToSessionIdWithDuration("1", "0");
		ResultActions action = this.mockMvc.perform(startVotingSessionRequest);
		expectBadRequestWhenDurationIsInvalid(action);
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestWhenDurationIsNotANumber() throws Exception { 
		final MockHttpServletRequestBuilder startVotingSessionRequest = startVotingSessionRequestToSessionIdWithDuration("1", "a");
		ResultActions action = this.mockMvc.perform(startVotingSessionRequest);
		expectBadRequestWithMessage(action, "durationInMinutes: expected to be Integer but was String");
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestIfVotingSessionIsAlreadyStarted() throws Exception { 

		final VotingSession votingSession = defaultVotingSession();
		when(service.startSession(anyInt(), anyInt())).thenThrow(new VotingSessionAlreadyStartedException(votingSession));
		final String expectedErrorMessage = String.format("Could not start voting session [%s] because it was started in %s", votingSession.getId(), votingSession.getStarting());

		final ResultActions action = this.mockMvc.perform(startVotingSessionRequestToSessionId("1"));

		expectBadRequestWithMessage(action, expectedErrorMessage);
	}

	@Test
	public void startVotingSessionShouldReturnNotFoundIfVotingSessionCouldNotBeFound() throws Exception { 

		when(service.startSession(anyInt(), anyInt())).thenThrow(new VotingSessionNotFoundException(DEFAULT_VOTING_SESSION_ID));
		final String expectedErrorMessage = String.format("Voting session [%s] could not be found", DEFAULT_VOTING_SESSION_ID);

		this.mockMvc.perform(startVotingSessionRequestToSessionId("1"))
			.andExpect(status().isNotFound())
			.andExpect(content().json(notFoundResponse(expectedErrorMessage), true));
	}

	@Test
	public void startVotingSessionShouldReturnBadRequestIfVotingSessionIdIsNotANumber() throws Exception { 

		this.mockMvc.perform(startVotingSessionRequestToSessionId("str-voting-session-id_(should be int)"))
			.andExpect(status().isBadRequest())
			.andExpect(content().json(badRequestResponse("id: expected to be Integer but was String"), true));
	}

	@Test
	public void shouldVoteYes() throws Exception {
		performVoteWithValidOptionAndExpectSucess(YES);
	}
	
	private void performVoteWithValidOptionAndExpectSucess(String validOption) throws Exception, JSONException {
		this.mockMvc.perform(defaultVoteRequestWithOption(validOption))
			.andExpect(status().isOk())
			.andExpect(content().json(voteResponseFor(validOption), true));
	}

	private RequestBuilder defaultVoteRequestWithOption(String option) throws Exception {
		final String voteUri = defaultVoteUri();
		return basePostRequestTo(voteUri).content(voteFor(option));
	}

	private String voteFor(String option) throws JSONException {
		final JSONObject node = new JSONObject().
				put(ASSOCIATE_ID_FIELD, DEFAULT_ASSOCIATE_ID).
				put(OPTION_FIELD, option);
		return node.toString(2);
	}

	private String voteResponseFor(String option) throws JSONException {
		JSONObject node = new JSONObject().put(OPTION_FIELD, option);
		return node.toString(2);
	}

	private String defaultVoteUri() {
		return String.format(VOTE_URI_TEMPLATE, DEFAULT_VOTING_SESSION_ID);
	}

	@Test
	public void shouldVoteNo() throws Exception {
		performVoteWithValidOptionAndExpectSucess(NO);
	}

	@Test
	public void votingForOptionOtherThanYesOrNoShouldReturnBadRequest() throws Exception {

		final String badRequestJson = ControllerUtils.badRequestResponse("option: {vote.option.invalid}");

		this.mockMvc.perform(defaultVoteRequestWithOption("something-else")).
			andExpect(status().isBadRequest()).
			andExpect(content().json(badRequestJson, true));
	}

	@Test
	public void votingForClosedVotingSessionShouldReturnBadRequest() throws Exception {

		when(service.registerVote(any())).thenThrow(new VotingSessionClosedException(DEFAULT_VOTING_SESSION_ID));
		final String badRequestJson = ControllerUtils.badRequestResponse(String.format("Voting session %s is closed", DEFAULT_VOTING_SESSION_ID));

		this.mockMvc.perform(basePostRequestTo(defaultVoteUri()).content(voteFor(YES)))
			.andExpect(status().isBadRequest())
			.andExpect(content().json(badRequestJson, true));
	}

	@Test
	public void votingForVotingSessionThatIsNotStartedShouldReturnBadRequest() throws Exception {

		when(service.registerVote(any())).thenThrow(new VotingSessionNotStartedException(DEFAULT_VOTING_SESSION_ID));
		final String badRequestJson = ControllerUtils.badRequestResponse(String.format("Voting session %s was not started", DEFAULT_VOTING_SESSION_ID));

		this.mockMvc.perform(basePostRequestTo(defaultVoteUri()).content(voteFor(YES)))
			.andExpect(status().isBadRequest())
			.andExpect(content().json(badRequestJson, true));
	}

	@Test
	public void votingMoreThanOnceInTheSameVotingSessionShouldReturnForbidden() throws Exception {

		when(service.registerVote(any())).thenThrow(new AssociateAlreadyVotedException(DEFAULT_VOTING_SESSION_ID, DEFAULT_ASSOCIATE_ID, new RuntimeException()));
		final String forbiddenJson = ControllerUtils.forbiddenResponse(String.format("Associate %s already has a vote registered in voting session %s", DEFAULT_ASSOCIATE_ID, DEFAULT_VOTING_SESSION_ID));

		this.mockMvc.perform(basePostRequestTo(defaultVoteUri()).content(voteFor(YES)))
			.andExpect(status().isForbidden())
			.andExpect(content().json(forbiddenJson, true));
	}

	@Test
	public void shouldRegisterVote() throws Exception {

		String voteResponseJson = new JSONObject().put(OPTION_FIELD, YES).toString(2);
		
		when(service.registerVote(any())).thenReturn(new Vote(DEFAULT_VOTING_SESSION_ID, DEFAULT_ASSOCIATE_ID, YES));

		this.mockMvc.perform(basePostRequestTo(defaultVoteUri()).content(voteFor(YES)))
			.andExpect(status().isOk())
			.andExpect(content().json(voteResponseJson, true));
	}

	@Test
	public void votingShouldReturnBadRequestIfVotingSessionIdIsNotANumber() throws Exception { 

		String voteUri = String.format(VOTE_URI_TEMPLATE, "str-voting-session-id_(should be int)");
		this.mockMvc.perform(basePostRequestTo(voteUri).content(voteFor(YES)))
			.andExpect(status().isBadRequest())
			.andExpect(content().json(badRequestResponse("id: expected to be Integer but was String"), true));
	}

	@Test
	public void shouldGetPartialResult() throws Exception { 
		
		final String optionA = "option-A";
		long totalA = 3L;
		final String optionB = "option-B";
		long totalB = 5L;

		final VotingSession votingSession = defaultVotingSession();
		final VotingSessionResult partials = new VotingSessionResult(votingSession);
		partials.addResult(optionA, totalA);
		partials.addResult(optionB, totalB);
		
		when(service.countVotes(anyInt())).thenReturn(partials);

		final String jsonResponse = new JSONObject()
				.put(VOTING_SESSION_FIELD, defaultVotingSessionDTOJsonObject())
				.put(TOTALS_FIELD, new JSONObject()
						.put(optionA, totalA)
						.put(optionB, totalB)).toString(2);
		
		this.mockMvc.perform(get(String.format(VOTING_SESSION_RESULT_URI_TEMPLATE, DEFAULT_VOTING_SESSION_ID)))
			.andExpect(status().isOk())
			.andExpect(content().json(jsonResponse, true));
	}

	@Test
	public void associateUnableToVoteShouldReturnNotFound() throws Exception {

		when(service.registerVote(any())).thenThrow(new AssociateUnableToVoteException(DEFAULT_ASSOCIATE_ID));
		final String notFoundJson = notFoundResponse(String.format("Associate %s is unable to vote", DEFAULT_ASSOCIATE_ID));

		this.mockMvc.perform(basePostRequestTo(defaultVoteUri()).content(voteFor(YES)))
			.andExpect(status().isNotFound())
			.andExpect(content().json(notFoundJson, true));
	}

	@Test
	public void unparseableJsonShouldReturnBadRequest() throws Exception {

		final String badRequestJson = badRequestResponse("Unrecognized token 'not': was expecting 'null', 'true', 'false' or NaN");

		this.mockMvc.perform(basePostRequestTo(defaultVoteUri()).content("not-json"))
			.andExpect(status().isBadRequest())
			.andExpect(content().json(badRequestJson, true));
	}

}
