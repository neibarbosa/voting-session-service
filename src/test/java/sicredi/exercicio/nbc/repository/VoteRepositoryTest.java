package sicredi.exercicio.nbc.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql=false)
public class VoteRepositoryTest {

	@Autowired
    private VoteRepository repository;
	
	@Test
	@Sql("setup_voting_session_with_votes.sql")
	public void testFindByVotingSessionId() {
		assertThat(repository.findByVotingSessionId(1)).hasSize(5);
	}

}
