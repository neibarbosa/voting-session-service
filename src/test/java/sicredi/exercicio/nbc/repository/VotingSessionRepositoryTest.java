package sicredi.exercicio.nbc.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(showSql=false)
public class VotingSessionRepositoryTest {

	@Autowired
    private VotingSessionRepository repository;
	
	@Test
	@Sql("setup_voting_session.sql")
	public void testFindByClosingBeforeAndPublished() {
		LocalDateTime checkInstant = LocalDateTime.of(2019, 3, 2, 10, 0, 0);
		assertThat(repository.findByClosingBeforeAndPublished(checkInstant, false)).hasSize(3);
		assertThat(repository.findByClosingBeforeAndPublished(checkInstant, true)).hasSize(1);
	}

}
