package sicredi.exercicio.nbc.service;

import static com.github.tomakehurst.wiremock.client.WireMock.badRequest;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.notFound;
import static com.github.tomakehurst.wiremock.client.WireMock.ok;
import static com.github.tomakehurst.wiremock.client.WireMock.serverError;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.net.URI;

import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;

import sicredi.exercicio.nbc.config.RestTemplateConfiguration;
import sicredi.exercicio.nbc.exception.UnableToAssertAssociateEligibilityException;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes={
		EligibilityService.class, 
		RestTemplateConfiguration.class
	})
@JsonTest
public class EligibilityServiceTest {

	private static final String ASSOCIATE_ID = "associate";
	private static final String SERVICE_URL = "/users/associate";
	
	@ClassRule
	public static WireMockClassRule wireMockRule = new WireMockClassRule(8089);

	@Rule
	public WireMockClassRule instanceRule = wireMockRule;

	@Autowired
	private EligibilityService service;
	
	@SpyBean
	private RestTemplate restTemplate;
	
	@Test
	public void associateIsEligibleIfServiceReturns200() {
		stubForStatus(ok());
		assertThat(service.isEligible(ASSOCIATE_ID)).isTrue();
	}

	private void stubForStatus(ResponseDefinitionBuilder status) {
		stubFor(get(urlEqualTo(SERVICE_URL)).willReturn(status));
	}

	@Test
	public void associateIsNotEligibleIfServiceReturns400() {
		stubForStatus(badRequest());
		assertThat(service.isEligible(ASSOCIATE_ID)).isFalse();
	}

	@Test
	public void associateIsNotEligibleIfServiceReturns404() {
		stubForStatus(notFound());
		assertThat(service.isEligible(ASSOCIATE_ID)).isFalse();
	}

	@Test
	public void internalServerErrorRaisesUnableToAssertAssociateEligibilityException() {
		stubForStatus(serverError());
		
		final UnableToAssertAssociateEligibilityException exception = 
				catchThrowableOfType(
						() -> service.isEligible(ASSOCIATE_ID), 
						UnableToAssertAssociateEligibilityException.class
				);

		assertThat(exception).hasCauseInstanceOf(HttpServerErrorException.class);
		assertThat(exception.getAssociateId()).isEqualTo(ASSOCIATE_ID);
	}

	@Test
	public void anyOtherExceptionRaisesUnableToAssertAssociateEligibilityException() {

		doThrow(RuntimeException.class).when(restTemplate).getForEntity(any(URI.class), any());
		
		final UnableToAssertAssociateEligibilityException exception = 
				catchThrowableOfType(
						() -> service.isEligible(ASSOCIATE_ID), 
						UnableToAssertAssociateEligibilityException.class
				);

		assertThat(exception).hasCauseInstanceOf(RuntimeException.class);
		assertThat(exception.getAssociateId()).isEqualTo(ASSOCIATE_ID);
	}

}