package sicredi.exercicio.nbc.service;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static sicredi.exercicio.nbc.test.VotingSessionTestingUtils.defaultVotingSession;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.kafka.core.KafkaTemplate;

import sicredi.exercicio.nbc.beans.ObjectMapperWrapper;
import sicredi.exercicio.nbc.domain.VotingSessionResult;

@RunWith(MockitoJUnitRunner.class)
public class VotingResultPublisherTest {

	@InjectMocks
	private VotingResultPublisher publisher;

	@Mock
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Mock
	private ObjectMapperWrapper mapper;

	@Captor
	private ArgumentCaptor<String> stringCaptor;
	
	@Test
	public void testObjectPublishedToKafka() {
		
		final VotingSessionResult result = new VotingSessionResult(defaultVotingSession());
		result.addResult("option", 1L);
		when(mapper.writeValueAsString(result)).thenReturn("some-json-string");

		publisher.sendVotingResult(result, "some-topic");

		verify(kafkaTemplate).send("some-topic", "some-json-string");
	}

}
