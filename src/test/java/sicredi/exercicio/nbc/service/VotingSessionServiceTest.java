package sicredi.exercicio.nbc.service;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.Assertions.catchThrowableOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static sicredi.exercicio.nbc.test.VotingSessionTestingUtils.DEFAULT_AGENDA;
import static sicredi.exercicio.nbc.test.VotingSessionTestingUtils.DEFAULT_VOTING_SESSION_ID;
import static sicredi.exercicio.nbc.test.VotingSessionTestingUtils.defaultVotingSession;
import static sicredi.exercicio.nbc.utils.VotingSessionConstants.BR_ZONE_ID;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;

import sicredi.exercicio.nbc.config.AppConfig;
import sicredi.exercicio.nbc.domain.VotingSessionResult;
import sicredi.exercicio.nbc.domain.VotingSession;
import sicredi.exercicio.nbc.dto.Vote;
import sicredi.exercicio.nbc.entity.VoteEntity;
import sicredi.exercicio.nbc.exception.AssociateAlreadyVotedException;
import sicredi.exercicio.nbc.exception.AssociateUnableToVoteException;
import sicredi.exercicio.nbc.exception.VotingSessionAlreadyStartedException;
import sicredi.exercicio.nbc.exception.VotingSessionClosedException;
import sicredi.exercicio.nbc.exception.VotingSessionNotFoundException;
import sicredi.exercicio.nbc.exception.VotingSessionNotStartedException;
import sicredi.exercicio.nbc.repository.VoteRepository;
import sicredi.exercicio.nbc.repository.VotingSessionRepository;

@RunWith(MockitoJUnitRunner.class)
public class VotingSessionServiceTest {

	private static final String DEFAULT_ASSOCIATE_ID = "associate";

	private static final int DEFAULT_DURATION_IN_MINUTES = 19;

	private static final String TOPIC_NAME = "topic";
	
	@InjectMocks
	private VotingSessionService service;
	
	@Mock
	private AppConfig appConfig;

	@Mock
	private VotingSessionRepository votingSessionRepository;

	@Mock
	private VoteRepository voteRepository;

	@Mock
	private VotingResultPublisher resultPublisher;

	@Mock
	private EligibilityService eligibilityService;

	@Captor
	private ArgumentCaptor<VotingSession> votingSessionCaptor;

	@Before
	public void setUp() {
		when(eligibilityService.isEligible(anyString())).thenReturn(true);
		when(appConfig.getVotingResultsTopic()).thenReturn(TOPIC_NAME);
	}
	
	@Test
	public void testCreateSession() {
		final String agenda = DEFAULT_AGENDA;
		final VotingSession unsaved = new VotingSession(agenda);
		final VotingSession saved = mock(VotingSession.class);
		when(votingSessionRepository.save(any())).thenReturn(saved);

		VotingSession actual = service.createSession(unsaved);

		verify(votingSessionRepository).save(votingSessionCaptor.capture());
		assertThat(votingSessionCaptor.getValue().getAgenda()).isEqualTo(agenda);
		verifyNoMoreInteractions(votingSessionRepository);
		assertThat(actual).isSameAs(saved);
	}

	@Test
	public void createSessionTrimsAgenda() throws Exception {
		final String agenda = surroundWithSpaces(DEFAULT_AGENDA);
		final VotingSession unsaved = new VotingSession(agenda);
		final VotingSession saved = mock(VotingSession.class);
		when(votingSessionRepository.save(any())).thenReturn(saved);

		VotingSession createdSession = service.createSession(unsaved);

		verify(votingSessionRepository).save(votingSessionCaptor.capture());
		assertThat(votingSessionCaptor.getValue().getAgenda()).isEqualTo(agenda.trim());
		verifyNoMoreInteractions(votingSessionRepository);
		assertThat(createdSession).isSameAs(saved);
	}

	private String surroundWithSpaces(String agenda) {
		return StringUtils.SPACE + agenda + StringUtils.SPACE;
	}

	@Test
	public void startVotingSessionThatIsNotStarted() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = new VotingSession(DEFAULT_AGENDA);
		votingSession.setId(sessionId);

		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.of(votingSession));
		when(votingSessionRepository.save(any())).thenAnswer(inv -> inv.getArgument(0));

		VotingSession startedVotingSession = service.startSession(sessionId, DEFAULT_DURATION_IN_MINUTES);

		final LocalDateTime starting = startedVotingSession.getStarting();
		final LocalDateTime closing = startedVotingSession.getClosing();
		assertThat(starting.until(closing, ChronoUnit.MINUTES)).isEqualTo(DEFAULT_DURATION_IN_MINUTES);
		assertThat(startedVotingSession)
			.isEqualToIgnoringGivenFields(votingSession, "starting", "closing");
	}

	@Test
	public void startVotingSessionThatIsAlreadyStarted() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = new VotingSession(DEFAULT_AGENDA);
		votingSession.setId(sessionId);
		votingSession.setStarting(LocalDateTime.now());

		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.of(votingSession));

		final VotingSessionAlreadyStartedException exception = 
				catchThrowableOfType(
						() -> service.startSession(sessionId, DEFAULT_DURATION_IN_MINUTES), 
						VotingSessionAlreadyStartedException.class
				);

		assertThat(exception.getVotingSession()).isSameAs(votingSession);
	}

	@Test
	public void startVotingSessionShouldThrowExceptionIfItCannotBeFound() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.empty());

		final VotingSessionNotFoundException exception = 
				catchThrowableOfType(
						() -> service.startSession(sessionId, DEFAULT_DURATION_IN_MINUTES), 
						VotingSessionNotFoundException.class
				);

		assertThat(exception.getId()).isSameAs(sessionId);
	}

	@Test
	public void registerVoteShouldThrowExceptionIfVotingSessionIsNotStarted() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = new VotingSession(DEFAULT_AGENDA);
		votingSession.setId(sessionId);

		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.of(votingSession));

		final VotingSessionNotStartedException exception = 
				catchThrowableOfType(
						() -> service.registerVote(new Vote(sessionId, "associate", "option")), 
						VotingSessionNotStartedException.class
				);

		assertThat(exception.getId()).isSameAs(sessionId);
	}

	@Test
	public void registerVoteShouldThrowExceptionIfVotingSessionIsClosed() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = new VotingSession(DEFAULT_AGENDA);
		votingSession.setId(sessionId);
		LocalDateTime starting = LocalDateTime.now(BR_ZONE_ID).minusHours(2);
		votingSession.setStarting(starting);
		votingSession.setClosing(starting.plusHours(1));

		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.of(votingSession));

		final VotingSessionClosedException exception = 
				catchThrowableOfType(
						() -> service.registerVote(new Vote(sessionId, DEFAULT_ASSOCIATE_ID, "option")), 
						VotingSessionClosedException.class
				);

		assertThat(exception.getId()).isSameAs(sessionId);
	}

	@Test
	public void registerVoteShouldThrowExceptionIfVotingSessionCannotBeFound() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.empty());

		final VotingSessionNotFoundException exception = 
				catchThrowableOfType(
						() -> service.registerVote(new Vote(sessionId, DEFAULT_ASSOCIATE_ID, "option")), 
						VotingSessionNotFoundException.class
				);

		assertThat(exception.getId()).isSameAs(sessionId);
	}

	@Test
	public void shouldRegisterVote() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = defaultVotingSession();

		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.of(votingSession));
		when(voteRepository.save(any())).thenAnswer(inv -> inv.getArgument(0));

		Vote vote = new Vote(sessionId, DEFAULT_ASSOCIATE_ID, "option");
		Vote actual = service.registerVote(vote);

		assertThat(actual).isEqualTo(vote);
	}

	@Test
	public void registerVoteShouldWrapDataIntegrityException() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = defaultVotingSession();

		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.of(votingSession));
		when(voteRepository.save(any())).thenThrow(DataIntegrityViolationException.class);

		final AssociateAlreadyVotedException exception = 
				catchThrowableOfType(
						() -> service.registerVote(new Vote(sessionId, DEFAULT_ASSOCIATE_ID, "option")), 
						AssociateAlreadyVotedException.class
				);

		assertThat(exception.getSessionId()).isEqualTo(DEFAULT_VOTING_SESSION_ID);
		assertThat(exception.getAssociateId()).isEqualTo(DEFAULT_ASSOCIATE_ID);
	}

	@Test
	public void uneligibleAssociateCannotVote() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = defaultVotingSession();

		when(eligibilityService.isEligible(anyString())).thenReturn(false);
		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.of(votingSession));


		final AssociateUnableToVoteException exception = 
				catchThrowableOfType(
						() -> service.registerVote(new Vote(sessionId, DEFAULT_ASSOCIATE_ID, "option")), 
						AssociateUnableToVoteException.class
				);

		verifyZeroInteractions(voteRepository);
		assertThat(exception.getAssociateId()).isEqualTo(DEFAULT_ASSOCIATE_ID);
	}

	@Test
	public void testMinimumDuration() {
		// with -1
		assertThatThrownBy(() -> service.startSession(DEFAULT_VOTING_SESSION_ID, -1))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessageStartingWith("duration must be greater than");

		// with 0
		assertThatThrownBy(() -> service.startSession(DEFAULT_VOTING_SESSION_ID, 0))
			.isInstanceOf(IllegalArgumentException.class)
			.hasMessageStartingWith("duration must be greater than");
	}

	@Test
	public void testPartialResult() {
		final VotingSession votingSession = defaultVotingSession();
		when(votingSessionRepository.findById(DEFAULT_VOTING_SESSION_ID)).thenReturn(Optional.of(votingSession));

		String optionA = "option-A";
		String optionB = "option-B";
		String optionC = "option-C";
		List<VoteEntity> votes = Arrays.asList(
				buildVoteForDefaultVotingSessionWithOption(optionA),
				buildVoteForDefaultVotingSessionWithOption(optionB),
				buildVoteForDefaultVotingSessionWithOption(optionC),
				buildVoteForDefaultVotingSessionWithOption(optionB),
				buildVoteForDefaultVotingSessionWithOption(optionA)
		);
		
		when(voteRepository.findByVotingSessionId(DEFAULT_VOTING_SESSION_ID)).thenReturn(votes);
		VotingSessionResult partialResult = service.countVotes(DEFAULT_VOTING_SESSION_ID);
		
		VotingSessionResult expected = new VotingSessionResult(votingSession);
		expected.addResult(optionA, 2L);
		expected.addResult(optionB, 2L);
		expected.addResult(optionC, 1L);
		assertThat(partialResult).isEqualTo(expected);
	}

	@Test
	public void partialResultShouldThrowExceptionIfVotingSessionCannotBeFound() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		when(votingSessionRepository.findById(sessionId)).thenReturn(Optional.empty());

		final VotingSessionNotFoundException exception = 
				catchThrowableOfType(
						() -> service.countVotes(sessionId), 
						VotingSessionNotFoundException.class
				);

		assertThat(exception.getId()).isSameAs(sessionId);
	}

	private VoteEntity buildVoteForDefaultVotingSessionWithOption(String option) {
		String randomAssociateId = RandomStringUtils.randomNumeric(11);
		VoteEntity voteEntity = new VoteEntity();
		voteEntity.setVotingSession(defaultVotingSession());
		voteEntity.setOption(option);
		voteEntity.setAssociateId(randomAssociateId);
		return voteEntity;
	}

	@Test
	public void testPublishResults_NoResultsToPublish() {
		service.publishResults();
		verify(votingSessionRepository).findByClosingBeforeAndPublished(any(), eq(false));
		verifyZeroInteractions(voteRepository, resultPublisher);
		verifyNoMoreInteractions(votingSessionRepository);
	}

	@Test
	public void testPublishResults_OneVotingSessionHasResultToPublish() {

		VotingSession defaultVotingSession = defaultVotingSession();
		when(votingSessionRepository
				.findByClosingBeforeAndPublished(any(), eq(false)))
				.thenReturn(
						singletonList(defaultVotingSession)
				);

		when(voteRepository
				.findByVotingSessionId(DEFAULT_VOTING_SESSION_ID))
				.thenReturn(
						singletonList(buildVoteForDefaultVotingSessionWithOption("optionA"))
				);
		
		service.publishResults();

		final VotingSessionResult expectedResult = new VotingSessionResult(defaultVotingSession);
		expectedResult.addResult("optionA", 1L);
		verify(resultPublisher).sendVotingResult(eq(expectedResult), eq(TOPIC_NAME));
		verify(voteRepository).findByVotingSessionId(DEFAULT_VOTING_SESSION_ID);
		verify(votingSessionRepository).save(votingSessionCaptor.capture());

		assertThat(votingSessionCaptor.getValue()).extracting(VotingSession::isPublished).isEqualTo(true);
	}

}
