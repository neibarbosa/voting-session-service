package sicredi.exercicio.nbc.test;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;

public class ControllerUtils {

	private static final String STATUS_FIELD = "status";
	private static final String ERRORS_FIELD = "errors";

	public static String badRequestResponse(String message) throws JSONException {
		return responseWithStatusAndMessage(HttpStatus.BAD_REQUEST, message);
	}

	public static String notFoundResponse(String message) throws JSONException {
		return responseWithStatusAndMessage(HttpStatus.NOT_FOUND, message);
	}

	public static String responseWithStatusAndMessage(HttpStatus status, String message) throws JSONException {
		JSONObject node = new JSONObject()
				.put(STATUS_FIELD, status.name())
				.put(ERRORS_FIELD, new JSONArray().put(message));
		return node.toString(2);
	}

	public static String forbiddenResponse(String message) throws JSONException {
		return responseWithStatusAndMessage(HttpStatus.FORBIDDEN, message);
	}
	
}
