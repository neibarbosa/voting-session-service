package sicredi.exercicio.nbc.test;

import static sicredi.exercicio.nbc.utils.VotingSessionConstants.BR_ZONE_ID;

import java.time.LocalDateTime;

import sicredi.exercicio.nbc.domain.VotingSession;

public class VotingSessionTestingUtils {

	public static final int DEFAULT_VOTING_SESSION_ID = 1;
	public static final String DEFAULT_AGENDA = "agenda";

	public static VotingSession defaultVotingSession() {
		int sessionId = DEFAULT_VOTING_SESSION_ID;
		final VotingSession votingSession = new VotingSession(DEFAULT_AGENDA);
		votingSession.setId(sessionId);
		final LocalDateTime starting = LocalDateTime.now(BR_ZONE_ID);
		votingSession.setStarting(starting);
		votingSession.setClosing(starting.plusHours(1));
		return votingSession;
	}
}
