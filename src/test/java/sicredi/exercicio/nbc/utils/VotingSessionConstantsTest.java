package sicredi.exercicio.nbc.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZoneId;

import org.junit.Test;

public class VotingSessionConstantsTest {

	@Test
	public void testBrZoneId() {

		assertThat(ZoneId.of("America/Sao_Paulo")).isEqualTo(VotingSessionConstants.BR_ZONE_ID);
	}

}
