insert into voting_session (id, agenda, starting, closing, version, published) 
values (1, 'agenda-01', '2019-03-02 08:00:00', '2019-03-02 09:00:00', 1, false);

insert into voting_session (id, agenda, starting, closing, version, published) 
values (2, 'agenda-02', '2019-03-02 08:00:00', '2019-03-02 09:10:00', 1, false);

insert into voting_session (id, agenda, starting, closing, version, published) 
values (3, 'agenda-03', '2019-03-02 08:00:00', '2019-03-02 09:20:00', 1, false);

insert into voting_session (id, agenda, starting, closing, version, published) 
values (4, 'agenda-04', '2019-03-02 08:00:00', '2019-03-02 19:00:00', 1, false);

insert into voting_session (id, agenda, starting, closing, version, published) 
values (5, 'agenda-05', '2019-03-02 08:00:00', '2019-03-02 19:00:00', 1, false);

insert into voting_session (id, agenda, starting, closing, version, published) 
values (6, 'agenda-06', '2019-03-02 08:00:00', '2019-03-02 09:00:00', 1, true);
