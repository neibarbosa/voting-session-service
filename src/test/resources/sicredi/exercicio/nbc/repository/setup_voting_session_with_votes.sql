insert into voting_session (id, agenda, version) values (1, 'some-agenda', 1);
insert into vote (voting_session_id, associate_id, option) values (1, 100, 'yes');
insert into vote (voting_session_id, associate_id, option) values (1, 101, 'no');
insert into vote (voting_session_id, associate_id, option) values (1, 102, 'yes');
insert into vote (voting_session_id, associate_id, option) values (1, 103, 'no');
insert into vote (voting_session_id, associate_id, option) values (1, 105, 'some-other-option');
